"""Module implementing ConicPGD and CpgdOptProblem classes. Other solvers (e.g.
Franck-Wolfe) could be added."""

import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


class CpgdOptProblem:
    """Abstract class for a cpgd optimization problem.

    It only needs to implement a compute_conic_gradient(), a
    cpgd_loss_function() and a param_retraction() method
    """

    def __init__(self) -> None:
        pass

    def compute_j_prime_and_its_gradient_at_particle_k(self, k, rs, params):
        # a scalar
        j_prime_k = None
        # to implement as dict for each param type (e.g. means, var, etc.)
        grad_j_prime_k = {}
        return (j_prime_k, grad_j_prime_k)

    def cpgd_loss_function(self, rs, params):
        pass

    def param_retraction(self, params, grad_params):
        pass


class ConicPGD(object):
    """Helper class to store the result of a conic particle gradient descent
    algorithm. Some visualization methods are available to plot loss or
    particle evolution.
    """

    def __init__(
        self,
        opt_problem,
        n_particles,
    ) -> None:
        if not isinstance(opt_problem, CpgdOptProblem):
            raise ValueError(
                "The optimization problem must inherit from class CpgdOptProblem."
            )
        self.opt_problem = opt_problem
        self.n_particles = n_particles

        self.n_iter = None
        self.init = None
        self.mirror_retraction = None
        self.step_sizes = None

    def optimize(self, n_iter, init, mirror_retraction, step_sizes):
        self.n_iter = n_iter
        self.init = init
        self.mirror_retraction = mirror_retraction
        self.step_sizes = step_sizes

        rs, params = init.values()
        rs_evolution = np.zeros((self.n_particles, n_iter))
        # create correctly shaped array for tracking all the parameter
        param_names = params.keys()
        params_evolution = {
            key: np.zeros((*param.shape, n_iter))
            for key, param in params.items()
        }
        # track loss function
        self.losses_ = np.nan * np.empty(n_iter)
        # - begin cpgd
        for i in range(n_iter):
            # store quantitities for plotting
            rs_evolution[:, i] = rs
            for key in param_names:
                params_evolution[key][..., i] = params[key]
            self.losses_[i] = self.opt_problem.cpgd_loss_function(rs, params)

            grad_r = np.zeros((self.n_particles,))
            grad_params = []  # TODO: avoid a list ?
            for k in range(self.n_particles):
                (
                    j_prime_k,
                    grad_j_prime_k,
                ) = self.opt_problem.compute_j_prime_and_its_gradient_at_particle_k(
                    k, rs, params
                )
                # - Riemanian gradient w.r.t. to r_k
                # If we use mirror_retraction we avoid multiplying by r_k
                # so that we don't re-divide after (avoid overflow when r_k is
                # small).
                grad_r_k = 2 * step_sizes["rs"] * j_prime_k
                if not mirror_retraction:
                    grad_r_k = rs[k] * grad_r_k

                grad_r[k] = grad_r_k
                grad_params.append(
                    {
                        param_name: step_sizes[param_name]
                        * grad_j_prime_k[param_name]
                        for param_name in param_names
                    }
                )

            # -- Compute the retractions (gradient step)
            # N.B. : the step_sizes are already inside gradients
            if mirror_retraction:
                # mirror retraction
                rs = rs * np.exp(-grad_r)
            else:
                # canonical retraction
                rs = rs - grad_r

            # use the parameter retraction implemented by opt_problem
            params = self.opt_problem.param_retraction(params, grad_params)

            # set negative rs to 0 ?
            # rs[np.where(rs < 0)] = 0

        self.amplitudes_ = rs**2 / self.n_particles
        self.rs_evolution = rs_evolution
        self.params_evolution = params_evolution
        self.params_ = params
        self.params_names_ = params.keys()

        return rs, params

    def _plot_loss(
        self,
        ax=None,
        xscale="log",
        title=None,
    ):
        if title is None:
            title = f"Loss evolution during CPGD"

        if ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)

        ax.plot(np.arange(self.n_iter), self.losses_, "r")
        ax.set_xscale(xscale)
        ax.title.set_text(title)

    def _plot_param(self, param_name, **kwargs):
        if param_name not in self.params_names_:
            raise ValueError(
                f"param_name argument should be one of {self.params_names_}, you gave {param_name}."
            )
        if self.params_[param_name].shape[1] == 1:
            self._plot_param1d(param_name, **kwargs)
        elif self.params_[param_name].shape[1] == 2:
            self._plot_param2d(param_name, **kwargs)
        else:
            ValueError("Dimension must be 1 or 2 to plot particle evolution.")

    def _plot_param1d(
        self, param_name, true_params=None, sample=None, ax=None
    ):
        """
        Plot the particle evolution in the (\mu_k, r_k) plane.
        The true parameters must be given.

        Parameters
        ----------
        true_params : a tuple of size 2
            The true parameters
        ax : plt.axes, optional
            the axe where the figure should be plotted, by default None
        """
        if ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)

        # plot 1D density estimation of the sample in the background
        if sample is not None:
            sns.kdeplot(
                sample, alpha=0.5, fill=False, label="Sample KDE", ax=ax
            )
            ax.patch.set_facecolor("white")

        ax.scatter(
            self.params_evolution[param_name][:, 0, 0],
            self.rs_evolution[:, 0] ** 2 / self.n_particles,
            color="#1B9E77",
            marker="o",
            alpha=0.9,
            edgecolors="none",
            s=30,
            label="Init",
        )
        ax.scatter(
            self.params_evolution[param_name][:, 0, -1],
            self.rs_evolution[:, -1] ** 2 / self.n_particles,
            marker="o",
            color="#D95F02",
            alpha=1,
            edgecolors="none",
            s=50,
            label="Final",
        )

        ax.scatter(
            self.params_evolution[param_name],
            self.rs_evolution**2 / self.n_particles,
            color="black",
            marker=".",
            alpha=0.9,
            edgecolors="none",
            s=5,
        )
        if true_params is not None:
            true_amplitudes, true_means = true_params
            ax.spines["bottom"].set_visible(True)
            markerline, stemlines, baseline = ax.stem(
                true_means[:, 0],
                true_amplitudes,
                linefmt="darkblue",
                basefmt="none",
                use_line_collection=True,
                label="Truth",
            )

        ax.legend(loc="upper center", fancybox=True, framealpha=0.5)
        markerline.set_markerfacecolor("darkblue")
        markerline.set_markeredgecolor("darkblue")
        markerline.set_markersize(8)
        markerline.set_alpha(0.7)
        ax.axhline(y=0, color="black", alpha=0.25)
        ax.title.set_text("Particle evolution (" + "means" + ")")

    def _plot_param2d(
        self,
        param_name,
        true_params=None,
        sample=None,
        ax=None,
        size=50,
        cmap="viridis",
    ):
        """
        Plot the particle evolution in the (\mu_1, \mu_2) plane, with color
        depending on their weights.
        The true parameters must be given.

        Parameters
        ----------
        true_params : a tuple of size 2
            The true parameters
        ax : plt.axes, optional
            the axe where the figure should be plotted, by default None (a new figure is created)
        size : int, optional
            size of the points, by default 50
        cmap : str, optional
            the matplotlib colormap, by default "viridis"
        """
        plot_list = []
        K = self.n_particles
        if ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)

        # plot shaded density estimation of the sample in the background
        if sample is not None:
            sns.kdeplot(
                x=sample[:, 0], y=sample[:, 1], alpha=0.5, fill=True, ax=ax
            )
            ax.patch.set_facecolor("white")
            ax.set_xlabel("$x_1$", fontsize=15)
            ax.set_ylabel("$x_2$", fontsize=15)
            ax.collections[0].set_alpha(0)

        sc_start = ax.scatter(
            self.params_evolution[param_name][:, 0, 0],
            self.params_evolution[param_name][:, 1, 0],
            c=self.rs_evolution[:, 0] ** 2 / K,
            marker="o",
            # alpha=0.9,
            edgecolors="none",
            s=size,
            cmap=cmap,
            label="Init",
        )

        sc_stop = ax.scatter(
            self.params_evolution[param_name][:, 0, -1],
            self.params_evolution[param_name][:, 1, -1],
            c=self.amplitudes_,
            marker="x",
            # alpha=1,
            # edgecolors="none",
            s=self.amplitudes_ * 10 * size,
            cmap=cmap,
            label="Final",
        )
        # draw particle number on plot
        for k in range(self.n_particles):
            ax.annotate(
                f"{k+1}",
                (
                    self.params_evolution[param_name][k, 0, 0],
                    self.params_evolution[param_name][k, 1, 0],
                ),
                size="small",
                horizontalalignment="right",
                verticalalignment="top",
            )

        sc_evol = ax.scatter(
            self.params_evolution[param_name][:, 0],
            self.params_evolution[param_name][:, 1],
            c=self.rs_evolution**2 / K,
            marker=".",
            # alpha=0.3,
            edgecolors="none",
            s=size / 50,
            cmap=cmap,
        )
        plot_list = [sc_evol, sc_start, sc_stop]
        if true_params is not None:
            true_amplitudes, true_means = true_params

            # true labels
            sc_true = ax.scatter(
                true_means[:, 0],
                true_means[:, 1],
                c=true_amplitudes,
                alpha=0.9,
                marker="D",
                edgecolors="none",
                s=size + 10,
                cmap=cmap,
                label="Target",
            )
            plot_list.append(sc_true)

        for im in plot_list:
            im.set_clim(0, 1)

        ax.set_xlabel(r"$m_1$", fontsize=10)
        ax.set_ylabel(r"$m_2$", fontsize=10)

        ax.spines["bottom"].set_visible(True)
        ax.title.set_text(r"Particle evolution")
        cbar = fig.colorbar(sc_evol)
        cbar.set_label(r"Mixture weights")
        plt.legend(fancybox=True, framealpha=0.5)

    def _plot_amplitude_evolution(self, ax=None, title=None):
        """Plot the evolution of the amplitude a_k=self.rs_evolution**2 / K
        over the iteration of CPGD.
        """
        if ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)

        amplitudes_evol = self.rs_evolution**2 / self.n_particles
        ax.plot(amplitudes_evol.T, color="red")
        if title is None:
            title = "Evolution of the particle amplitudes $r_k^2/K$"
        ax.set_title(title)
        # draw particle number at the end
        for k in range(self.n_particles):
            ax.annotate(
                f"{k+1}",
                (self.n_iter, amplitudes_evol[k, -1]),
                size="small",
                horizontalalignment="right",
                verticalalignment="top",
            )
