"""Module implementing Gmm class for sampling from Gaussian Mixture ."""
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


class Gmm:
    """A class to simulate according to a Gaussian mixture model. Helper
    method are available for visualization in 1d and 2d.
    """

    def __init__(self, d=2, n_spikes=4, min_separation=1, seed=None):
        """The true means parameters are generated with a minimum separation.

        Parameters
        ----------
        d : int, optional
            dimension, by default 2
        n_spikes : int, optional
            number of mixture components, by default 4
        min_separation : int, optional
            the minimal separation between the means, by default 1
        seed : _type_, optional
            NumPy random seed, by default None
        """
        self.n_spikes = int(n_spikes)
        self.d = d
        self.min_separation = min_separation
        if isinstance(seed, int):
            self.seed = seed
            self.rg = np.random.default_rng(self.seed)
        else:
            self.seed = None
            self.rg = np.random.default_rng(None)

        self.amplitudes = None
        self.means = None
        self.sigmas = None

        self.n_sample = None
        self.sample = None
        self.clust = None

    def mixing_law(self, means=None, sigmas=None, amplitudes=None):
        """

        :param means: means of the Gaussian p.d.f
        :param sigmas: variances $\sigma_k^2$
        :param amplitudes: weight of the mixture
        :return: a GMM_1D object with parameters initialized
        """
        if amplitudes is not None:
            assert amplitudes.shape == (self.n_spikes,)
            assert np.sum(amplitudes > 0) == self.n_spikes

        if means is not None:
            assert means.shape == (self.n_spikes, self.d)

        if sigmas is not None:
            assert sigmas.shape == (self.n_spikes, self.d, self.d)

        # Amplitudes
        if amplitudes is None:
            self.amplitudes = self.rg.dirichlet(np.ones(self.n_spikes).ravel())
        else:
            self.amplitudes = amplitudes / np.sum(amplitudes)

        # Locations
        if means is None:
            self.means = self.rg.normal(
                loc=0, scale=self.min_separation, size=(self.n_spikes, self.d)
            )
        else:
            self.means = means

        # locations barycenter is zero
        # self.means = self.means - np.mean(self.means, axis=0)

        # variance
        if sigmas is None:
            self.sigmas = np.array([np.eye(d) for _ in range(self.n_spikes)])
        else:
            self.sigmas = sigmas

    def sampling(self, n=200, display=False):
        self.sample = np.zeros((n, self.d))
        self.n_sample = int(n)
        clust = self.rg.multinomial(1, pvals=self.amplitudes, size=n)
        self.clust = np.where(clust == np.amax(clust, axis=0))[1]
        for k in range(self.n_spikes):
            indk = np.where(self.clust == k)[0]
            self.sample[indk, :] = self.rg.multivariate_normal(
                mean=self.means[k, :],
                cov=self.sigmas[k, :, :],
                size=indk.shape[0],
            )
        if display and self.d <= 2:
            self.plot_distribution()

    def plot_distribution(self, show_component=False):
        if self.d == 1:
            plt.figure()
            sns.set_color_codes()
            sns.kdeplot(self.sample, fill=False, legend=False)
            if show_component:
                for k in range(self.n_spikes):
                    # TODO: change color and legend
                    sns.kdeplot(
                        self.sample[self.clust == k],
                        fill=True,
                        legend=False,
                        color=k,
                    )
        elif self.d == 2:
            # TODO
            raise Warning("TODO")
        else:
            raise ValueError("Cannot plot sample density when d > 2")
