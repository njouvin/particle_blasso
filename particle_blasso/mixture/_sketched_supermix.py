"""Module implementing SketchingOperator and SketchedSupermix classes
representing an S2Mix BLASSO problem."""
import time
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
from particle_blasso._solvers._ConicPGD import ConicPGD, CpgdOptProblem
from particle_blasso.data import Gmm

_IMPLEMENTED_ALGOS = ["cpgd"]


# --- Helper checking functions, name is self explanatory
def _verify_if_implemented_model(model):
    implemented_models = ["gmm"]
    if model.lower() not in implemented_models:
        raise NotImplementedError(
            f"Model {model} is not implemented yet."
            f"Try one of {implemented_models}"
        )
    return model


def _verify_if_implemented_sketching(sketching_distribution):
    implemented_sketching = ["uniform"]
    if sketching_distribution.lower() not in implemented_sketching:
        raise NotImplementedError(
            f"Sketching distribution {sketching_distribution} is not implemented yet."
            f"Try one of {implemented_sketching}"
        )
    return sketching_distribution


def _get_fourier_of_template_distribution(model):
    """Return the Fourier transform of the mixture model template distribution
    (denoted as phi).

    For now only Gaussian mixture are implemented."""
    if model == "gmm":

        def ft_phi(freqs, var):
            """Fourier transform of a centered Gaussian p.d.f with diagonal
            covariance sigma

            Parameters
            ----------
            freqs : (m, d) np.array
            var : (d, d) np.array

            Returns
            -------
            (m,) np.array
                fourier transform evaluated at the m `freqs` points.
            """
            # trick : diag(A @ B @ A.T) == sum(A @ B * A, axis = -1)
            return np.exp(-0.5 * (freqs @ var * freqs).sum(-1))

    else:
        raise NotImplementedError(
            f"Fourier transform of a {model} model is not implemented yet."
        )
    return ft_phi


def _draw_freqs_nd(rng, d, n_sketch, distribution="uniform", bandwidth=1):
    if distribution.lower() == "uniform":
        freqs = rng.uniform(low=-bandwidth, high=bandwidth, size=(n_sketch, d))
    elif distribution.lower() == "gaussian":
        freqs = rng.normal(loc=0, scale=bandwidth, size=(n_sketch, d))
    else:
        raise NotImplementedError(
            f"Distribution {distribution} is not implemented yet."
        )

    return freqs


class SketchingOperator:
    r"""
    Class defining a sketching operator :math:`A` on the sample space.
    The sketching distribution :math:`\Lambda`, its bandwith parameter and the number of sketch :math:`m` must be provided.

    The sketch of the dataset :math:`(x_1, \ldots, x_n)` is also known as its Random Fourier Features and is defined as the Fourier transform of the
    empirical distribution evaluated at randomly sampled frequencies:


    .. math::
        y = A(x_1, ..., x_n) =  \left(\frac{1}{n} \sum_{i=1}^n e^{-i \omega_j^T x_i} \right)_{j=1}^m \in \mathbb{C}^m, \quad \omega_j \sim \Lambda .
    """

    def __init__(
        self,
        sketching_distribution="Uniform",
        n_sketches=1e3,
        bandwidth=1,
        seed=12345,
    ) -> None:
        self.sketching_distribution = _verify_if_implemented_sketching(
            sketching_distribution
        )
        self.n_sketches = n_sketches
        self.bandwidth = bandwidth
        self.seed = seed
        self.rng = np.random.default_rng(seed=seed)
        self.data_sketch = None

    def fit_transform(self, X) -> None:
        r"""Compute the sketch of an n x d dataset. The sketch is an (n_sketches, 1) np.array and is stored as an attribute ``data_sketch``
        of the object.

        Parameters
        ----------
        X : np.array of shape (n, d)
            The data set to sketch
        """
        self.n, self.d = X.shape
        self.frequencies_ = _draw_freqs_nd(
            self.rng,
            self.d,
            n_sketch=self.n_sketches,
            distribution=self.sketching_distribution,
            bandwidth=self.bandwidth,
        )

        # Create Random Fourier features, a.k.a the sketch of the data
        # (in \mathbb{C}^n_sketch)
        self.data_sketch = (1 / self.n) * np.exp(
            -1j * self.frequencies_ @ X.T
        ).sum(axis=1)


def _signed_square(r):
    """Signed square function (useful for optim on signed measures -> sign
    attributed to each particle)

    See appendix A of Chizat
    """
    return np.abs(r) * r


class SketchedSupermix(CpgdOptProblem):
    r"""
    Class defining a Sketched Supermix (S2Mix) Beurling-Lasso problem defined on
    Radon measures by:

    .. math::
        {\arg\min}_\mu \frac{1}{2} \frac{\gamma}{m} \Vert A \mu - y\Vert_{\mathbb{C}^m}^2 + \kappa  \Vert \mu \Vert_{TV}
    """

    def __init__(
        self, sketcher, model="gmm", kappa_reg=1, variance=None, seed=12345
    ):
        r"""At initialization, the fourier transform of the template
        distribution is precomputed at the sampled frequencies.

        The normalization constant arising from the RKHS norm is computed as
        :math:`\gamma = \frac{1}{(2 \pi)^d}`.

        Parameters
        ----------
        sketcher : _type_
            _description_
        model : str, optional
            The template distribution of the mixture (e.g. Gaussian or Laplace
            mixture), by default "gmm"
        kappa_reg : int, optional
            The total variation regularization coefficient, by default 1
        variance : (d, d) np.array, optional
            The covariance matrix, by default None. If None, the covariance is
            to the identity np.eye(d)
        seed : int, optional
            numpy rng seed, by default 12345

        Raises
        ------
        ValueError
            If sketcher is not a SketchingOperator object
        ValueError
            If kappa_reg is not a positive int
        ValueError
            If variance is not of the correct shape or has non-positive values.
        """
        if not isinstance(sketcher, SketchingOperator):
            raise ValueError(
                "Sketched data arg must be of class `SketchingOperator`"
            )
        self.model = _verify_if_implemented_model(model)
        if isinstance(kappa_reg, float) and kappa_reg > 0:
            self.kappa_reg = kappa_reg
        else:
            raise ValueError(
                f"kappa_reg must be a positive float, {kappa_reg} was given"
            )

        self.n, self.d, self.n_sketches = (
            sketcher.n,
            sketcher.d,
            sketcher.n_sketches,
        )
        self.sketcher = sketcher
        self.y = sketcher.data_sketch

        if variance is None:
            variance = np.eye(self.d)
        elif variance.shape != (self.d, self.d) or np.any(variance <= 0):
            raise ValueError(
                f"`variance` must be a ({d},{d}) shaped np.array with positive"
                f"eigenvalues encoding a covariance matrix, "
                f"{variance.shape} was given."
            )

        self.variance = variance
        self.ft_phi_func = _get_fourier_of_template_distribution(model)
        self._ft_phi = self.ft_phi_func(sketcher.frequencies_, var=variance)

        # constant from RKHS's norm definition

        self.norm_cte = 1
        self.seed = seed
        self.random_generator = np.random.default_rng(seed)

        self.fitted_ = False  # not fitted yet

    def cpgd_loss_function(self, rs, params):
        r"""Loss function of the BLASSO problem for a discrete measure

        .. math::
            \mu_K = \frac{1}{K} \sum_{k=1}^K r_k^2 \delta_{m_k}

        Parameters
        ----------
        rs : (K,) np.array
            the square-root of amplitudes
        params: a dict
            with only one entry "means" containing the (K, d) np.array
            with the means

        Returns
        -------
        float
            the S2Mix objective funtion's value for the candidate point.
        """

        means = params["means"]
        n_particles = rs.size

        freqs = self.sketcher.frequencies_

        amplitudes = _signed_square(rs) / n_particles
        W = np.tile(amplitudes, (self.n_sketches, 1)).T
        M = means @ freqs.T
        current = self._ft_phi * np.sum(W * np.exp(-1j * M), axis=0)
        loss = (
            0.5
            * self.norm_cte
            * (1 / self.n_sketches)
            * (np.linalg.norm(current - self.sketcher.data_sketch)) ** 2
        )
        loss += self.kappa_reg * amplitudes.sum()

        return loss

    # Gradient computations
    def compute_j_prime_and_its_gradient_at_particle_k(self, k, rs, params):
        r"""Mandatory function for gradient descent. Compute :math:`J'_{\mu_K}(m_k)` and :math:`\nabla_m J'_{\mu_K}(m_k)` when :math:`\mu_K` is a discrete measure composed of K particles.

        The "conic gradients" is the standard euclidean gradient for
        :math:`r_k` while it is the euclidean gradient divided by :math:`r^2_k`
        for :math:`m_k`.

        Parameters
        ----------
        k : integer
            the particle index
        rs : (n_particles,) np.array
            the vector of (scaled & square-rooted) particle amplitudes
        params: a dict
            with only one entry "means",a (n_particles, d) np.array

        Returns
        -------
        2-tuple
            first coordinate is the gradient w.r.t. r_k and second w.r.t.
            :math:`m_k` (wrapped in a dictionnary for the ConicPGD solver)
        """
        means = params["means"]
        n_particles = rs.size
        freqs = self.sketcher.frequencies_
        amplitudes = _signed_square(rs) / n_particles
        means_centered_k = means - means[k, :]
        means_quad = freqs @ means_centered_k.T
        freqs_times_meank = freqs @ means[k, :].T
        # Extract complex modulus and angle of the data sketch
        sketch_modulus = np.absolute(self.sketcher.data_sketch)
        sketch_angle = np.angle(self.sketcher.data_sketch)  # in Radians

        # --- Compute J'_\nu(r_k, \mu_k)
        # quadratic part
        j_prime_k = np.sum(
            self._ft_phi**2 * np.sum(amplitudes * np.cos(means_quad), axis=1)
        )
        # linear part
        j_prime_k -= np.sum(
            self._ft_phi
            * sketch_modulus
            * np.cos(freqs_times_meank + sketch_angle)
        )
        # normalize
        j_prime_k *= self.norm_cte * (1 / self.n_sketches)
        j_prime_k += self.kappa_reg

        # Gradient of J'_\nu(r_k, \mu_k) w.r.t. \mu_k
        # quadratic part
        grad_j_prime_k = np.sum(
            (
                self._ft_phi**2
                * np.sum(amplitudes * np.sin(means_quad), axis=1)
            )[:, np.newaxis]
            * freqs,
            axis=0,
        )
        # linear part
        grad_j_prime_k += np.sum(
            (
                self._ft_phi
                * sketch_modulus
                * np.sin(freqs_times_meank + sketch_angle)
            )[:, np.newaxis]
            * freqs,
            axis=0,
        )
        # normalize
        grad_j_prime_k *= self.norm_cte * (1 / self.n_sketches)

        # need to return a dictionnary with entry "means" for grad_j_prime_k
        return j_prime_k, {"means": grad_j_prime_k}

    def param_retraction(self, params, grad_params):
        r"""The parameter retraction. Here the only parameter are the means
        and the retraction is the canonical one :
        :math:`x_{new} = x - \beta \nabla f(x)`

        **Warnings:** multiplication by the step_size :math:`\beta` is assumed to be already done in `grad_params`, as in the formulaes in Algorithm 1 of  Chizat (2020).

        Parameters
        ----------
        params : dict
            a dictionnary with only one entry "means" containing the current
            means
        grad_params : list of dictionnarie
            a list of size K containing the conic gradient w.r.t. the k-th means in a dictionnary.

        Returns
        -------
        dict
            A dictionnary {"means": means_new} with the value of the new means
            after the retraction (gradient step).
        """
        means_new = params["means"]
        grad_means = np.vstack([grad_k["means"] for grad_k in grad_params])
        # Use the standard retraction for the means (not a manifold)
        # N.B. grad_means is already multiplied by step_size["means"]
        means_new = means_new - grad_means

        # Eventually, project onto a constraint sets if the means grow too big
        max_m = 1e4
        means_norms = np.linalg.norm(means_new, axis=1)
        ind_constr_m = np.where(means_norms > max_m)[0]
        if ind_constr_m.size != 0:
            means_new[ind_constr_m, :] = (
                means_new[ind_constr_m, :] / means_norms[ind_constr_m, None]
            )
        return {"means": means_new}

    def fit(self, algorithm="cpgd", *args, **kwargs):
        r"""Solves the Sketched BLASSO to estimate models parameters :
        amplitudes and means.

        For now only conic particle gradient descent is available. Future work
        could implement Franck-Wolf.

        Parameters
        ----------
        algorithm : str, optional
            the algorithm to use to solve the sketched BLASSO problem, by
            default "cpgd"

        Raises
        ------
        ValueError
            If the required algorithm is not implemented.
        """
        if self.fitted_ is True:
            print(
                "The object has already been fitted. The particles are "
                "forgotten, re-fitting using a new initialisation."
            )
        if algorithm not in _IMPLEMENTED_ALGOS:
            raise ValueError(
                f"{algorithm} is not implemented. Try one of {_IMPLEMENTED_ALGOS}"
            )
        elif algorithm.lower() == "cpgd":
            self._fit_cpgd(*args, **kwargs)

        self.fitted_ = True

    def _fit_cpgd(
        self,
        n_particles,
        n_iter,
        init={"rs": None, "params": {"means": None}},
        mirror_retraction=False,
        step_sizes={"rs": 1, "means": 1},
    ):
        r"""Fit a SketchedSupermix object via the Conic Particle Gradient
        Descent (Chizat, 2019).

        Parameters
        ----------
        n_particles : int
            the number of particles
        n_iter : int
            the number of iterations
        init : dict, optional
            the initial values for amplitudes and means, by default {"rs": None, "params": {"means": None}}
        mirror_retraction : bool, optional
            should the algorithm use mirror retraction in gradient descent, by
            default False (canonical retraction is used)
        step_sizes : dict, optional
            the step size for the gradient computations w.r.t. to :math:`r` and :math:`\mu`, by default {"rs": 1, "means": 1}

        Returns
        -------
        None
            The object if fitted in place, and attributes self.rs_ and self.
            means_ created.

        Raises
        ------
        ValueError
            If the init["rs"] is of the wrong shape
        ValueError
            If the init['params']["means"] is of the wrong shape
        """
        # Check initialization
        if init["rs"] is not None and init["rs"].shape != (n_particles,):
            raise ValueError(
                f"The initial particles amplitude `rs` must be a"
                f"({n_particles},) shaped np.array, {init['rs'].shape} was"
                f"given."
            )
        if init["params"]["means"] is not None and init["params"][
            "means"
        ].shape != (
            n_particles,
            self.d,
        ):
            raise ValueError(
                f"The initial particle value `means` must be a"
                f"({n_particles},{self.d}) shaped np.array,"
                f"{init['params']['means'].shape} was given."
            )

        # If no custom initialization, create a random one
        if init["rs"] is None:
            init["rs"] = np.ones(n_particles) / n_particles

        if init["params"]["means"] is None:
            init["params"]["means"] = self.random_generator.normal(
                scale=10, size=(n_particles, self.d)
            )

        # Initialize the ConicPGD object
        self._algorithm = "CPGD"
        self.cpgd_ = ConicPGD(
            self,
            n_particles,
        )
        self.rs_, self.params_ = self.cpgd_.optimize(
            n_iter, init, mirror_retraction, step_sizes
        )

    def score(self, sample):
        r"""
        Minus the log-likelihood of the mixture induced by the found Sketched
        BLASSO solution.

        *Warnings*: For now, only GMM llhood is implemented.
        TODO: implement a _get_template_density(self.model) to generalize to any (translation invariant) mixture model.

        Parameters
        ----------
        sample : (n, d) np.array
            The sample points at which the log-llhood should be computed

        Returns
        -------
        float
            The log-llhood
        """

        w = (
            self.amplitudes_ / self.amplitudes_.sum()
        )  # normalize mixture weight to 1 for fair comparison
        mask = np.where(w >= 1e-100)[
            0
        ]  # don't keep particles with weights near 0
        logscore = np.full(
            shape=(sample.shape[0], self.cpgd_.n_particles), fill_value=-np.inf
        )
        for k in mask.tolist():
            logscore[:, k] = np.log(w[k]) + scipy.stats.multivariate_normal(
                mean=self.means_[k], cov=np.diag(self.variance)
            ).logpdf(sample)

        temp = np.max(logscore, axis=1)[:, np.newaxis]
        score = -np.sum(temp + np.log(np.sum(np.exp(logscore - temp))))

        return score

    def plot(self, type="param", **kwargs):
        """Generic plotting function, depending on ``type``, it calls the
        plotting method of the cpgd object

        Parameters
        ----------
        type : str, optional
            the plot type among ["param", "loss", "amplitudes"], by default "param"

        Raises
        ------
        ValueError
            If the model has not been fitted yet.
        """
        if self.fitted_ is False:
            raise ValueError(
                "Cannot plot loss or params evolution, the model"
                "has not been fitted yet !"
            )
        if self._algorithm.lower() == "cpgd":
            if type == "param":
                self.cpgd_._plot_param(**kwargs)
            elif type == "loss":
                self.cpgd_._plot_loss(**kwargs)
            elif type == "amplitudes":
                self.cpgd_._plot_amplitude_evolution(**kwargs)
            else:
                Warning("Plot type not implemented yet.")

    @property
    def means_(self):
        return self.params_["means"]

    @property
    def amplitudes_(self):
        return self.rs_**2 / self.cpgd_.n_particles

    @property
    def losses_(self):
        return self.cpgd_.losses_


if __name__ == "__main__":
    import seaborn as sns
    from sklearn.cluster import KMeans

    # seed for numpy default_rng instantiation
    seed = int(np.random.default_rng(None).random() * 1e8)
    seed = 42
    print("Seed = {}".format(seed))

    # Parameters
    n_spikes = 2
    d = 2
    min_separation = 10
    n = int(1e3)

    amplitudes = np.repeat(1, n_spikes) / n_spikes
    means = np.zeros((n_spikes, d))
    means[:, 0] = min_separation * np.arange(n_spikes)
    means[:, 1:] = np.random.normal(loc=0, scale=1, size=(n_spikes, d - 1))
    sigmas = np.array([np.eye(d) for _ in range(n_spikes)])
    gmm = Gmm(n_spikes=n_spikes, d=d, min_separation=min_separation, seed=seed)
    gmm.mixing_law(means=means, sigmas=sigmas, amplitudes=amplitudes)
    gmm.sampling(n=n, display=False)

    # --- Create Sketching Operator
    n_sketches = int(1000)
    sketch_dist = "uniform"
    bandwidth = 10
    uniform_sketcher = SketchingOperator(
        n_sketches=n_sketches,
        bandwidth=bandwidth,
        seed=seed,
        sketching_distribution=sketch_dist,
    )
    uniform_sketcher.fit_transform(X=gmm.sample)

    # --- Create the BLASSO problem
    kappa_reg = 1e-5
    model = "gmm"
    blasso = SketchedSupermix(
        sketcher=uniform_sketcher,
        model=model,
        kappa_reg=kappa_reg,
        seed=seed,
        variance=None,
    )

    # --- Fit with CPGD

    eta = 0.00001
    step_r = 0.1 * eta
    step_m = 0.5 * eta
    step_sizes = {"rs": step_r, "means": step_m}

    n_particles = 10
    n_iter = int(1000) * 2
    mirror_retraction = False

    # Init with Kmeans
    km = KMeans(n_clusters=n_particles, random_state=seed, n_init=10)
    km.fit(gmm.sample)
    mean_init = km.cluster_centers_

    _, r_init = np.unique(km.labels_, return_counts=True)
    r_init = np.sqrt(
        (r_init / gmm.n_sample) * n_particles
    )  # re-scale the particle weights

    t0 = time.time()
    blasso.fit(
        algorithm="cpgd",
        n_particles=n_particles,
        n_iter=n_iter,
        # init={"rs": r_init, "params": {"means": mean_init}},
        init={"rs": None, "params": {"means": None}},
        mirror_retraction=mirror_retraction,
        step_sizes=step_sizes,
    )

    t1 = time.time()
    if np.all(np.diff(blasso.losses_) < 0):  # check that loss is decreasing
        print(
            "Increasing objective, check step_sizes values for rs and means."
        )

    print("Overall running time for Sketch CPGD {0} seconds".format(t1 - t0))

    blasso.plot(type="loss")
    blasso.plot(
        type="param",
        param_name="means",
        true_params=(gmm.amplitudes, gmm.means),
        sample=gmm.sample,
    )
    blasso.plot(type="amplitudes")

    plt.show()
    # print('Score = {}'.format(res_sketch.score()))
    print(blasso.amplitudes_)
    print(blasso.amplitudes_.sum())
