============================================
S2Mix : the sketched supermix BLASSO problem
============================================


.. role:: raw-latex(raw)
   :format: latex
..

Introduction
============

Mixture estimation problem
--------------------------

Our goal is to recover the true parameters
:math:`\{(a_k^\star, m_k^\star)\}_{k=1, \ldots, K^\star}` of the
translation invariant mixture density

.. math::


   f^0 = \sum_{k=1}^{K^\star} a_k^\star \varphi(\cdot - m_k^\star)

from a sample :math:`X = (x_1, \ldots, x_n) \subset \mathbb{R}^d`. In
this notebook, :math:`\varphi` will be the centered Gaussian density
with known covariance matrix :math:`\Sigma` :
:math:`\mathcal{N}_d(0, \Sigma)`. However, it can be any symmetric
translation invariant density with know Fourier transform
:math:`\mathcal{F}[\varphi] = \sigma`.

S2Mix : Sketched Beurling-Lasso for mixture estimation
------------------------------------------------------

   This section is a very crude introduction to BLASSO, Supermix and its
   sketching version S2Mix in a very concise fashion. For more a more
   detailed introduction please refer to the preprint.

Introducing the convolution operator
:math:`\Phi : \mu \mapsto \varphi \star \mu`, one can write
:math:`f^0 = \Phi \mu^\star` and the parameter estimation problem above
may be cast as recovering the true **sparse** measure on the parameter
space.

.. math:: \mu^\star = \sum_{k=1}^{K^\star} a_k^\star \delta_{m_k^\star}

Differing from standard approaches such as *e.g.* maximum-likelihood
estimation, a classical estimator of :math:`\mu^\star` is obtained as
the solution of an infinite-dimensional analog to the Lasso regression
over the space of finite Radon measures
:math:`\mathcal{M}(\mathbb{R}^d)`. The latter is known as the
Beurling-LASSO `(BLASSO, De Castro et.
al. 2012) <https://ydecastro.github.io/research/paper4.pdf>`__ and is
defined as

.. math::  \hat{\mu} \in \underset{\mu}{\arg\min}\frac{1}{2} \Vert y - F \mu \Vert_{\mathbb{F}}^2 + \kappa \Vert \mu \Vert_{TV}.

Here, :math:`y` represents a sample embedding , while :math:`F` is a
linear operator from the Radon measures to some Hilbert space
:math:`\mathbb{F}` to be chosen. The BLASSO enjoys several properties as
an optimization problem, in particular convexity allowing to leverage
duality for theoretical analysis.

Recently, `De Castro, Gadat, Marteau and
Maugis <https://hal.science/hal-02190117>`__ proposed *Supermix* which
compares the fourier transform of the sample empirical distribution
:math:`\hat{\delta}_n` to the one of a candidate :math:`\Phi \mu` in a
weighted :math:`L^2(\Lambda)` space, where :math:`\Lambda` is some
probability measure with density w.r.t to the lebesgues measure.

.. math::


   \begin{equation}
    \hat{\mu} \in \underset{\mu}{\arg\min} \frac{1}{2} \frac{1}{(2\pi)^d} \int_{\mathbb{R}^d} \vert \mathcal{F}[\hat{\delta}_n] - \sigma \mathcal{F}[\mu] \vert^2(\omega) \Lambda(\omega) d \omega  + \kappa \Vert \mu \Vert_{TV} \tag{Supermix}
   \end{equation}

They propose to take :math:`\Lambda \propto Unif([-\eta, \eta])` where
:math:`\eta` is some bandwidth parameter such that
:math:`[-\eta, \eta] \subset \textrm{Supp}(\sigma)`.

However, computing the data fitting term (and hence the loss or its
gradient) for any candidate :math:`\mu` requires solving
:math:`d`-dimensional integrals w.r.t to :math:`\Lambda`, which is
impractical. Hence, we proposose to resort to Monte-Carlo approximation
of these integrals with :math:`m` samples
:math:`(\omega_1, \ldots, \omega_m)` drawn *i.i.d.* from
:math:`\Lambda`. This leads to another Beurling-Lasso problem, called
sketched-Supermix (S2Mix) with \* Hilbert space :
:math:`\mathbb{F} = (\mathbb{C}^m, \sqrt{\frac{\gamma}{m}} \Vert \cdot \Vert_{\mathbb{C}^m})`
\* Sample embedding :
:math:`y = \left(\frac{1}{n} \sum_{i=1}^n e^{-i \omega_j^\top x_i} \right)_{j=1}^m`
\* Linear operator :
:math:`F(\mu) = \left( \sigma(\omega_j) \mathcal{F}[\mu](\omega_j) \right)_{j=1}^m`,
where we recall that :math:`\sigma = \mathcal{F}[\varphi]`.

The *sketching* terminology comes from the fact that the
:math:`n \times d` dataset :math:`X` is reduced to an :math:`m`
dimensional random vector :math:`y` (the *sketch*) which can be
precomputed once and for all, which is relevant for saving both
computations and memory `(Gribonval et.
al. 2021) <https://inria.hal.science/hal-02909766v2/document>`__. Here,
the so-called sketching operator is
:math:`\mathcal{A}_m : \nu \mapsto (\mathcal{F}[\nu](\omega_j))_{j=1}^m`,
also known as Random Fourier Feature (RFFs) since the fourier transform
is only measured at randomly sampled frequencies. Moreover, we can
rewrite :math:`F = \mathcal{A}_m \circ \Phi` and the S2Mix program

.. math::


   \begin{equation}
   \hat{\mu} \in \underset{\mu}{\arg\min} \left\{ J(\mu) := \frac{\gamma}{m} \frac{1}{2} \Vert \mathcal{A}_m \hat{\delta}_n - \mathcal{A}_m \circ \Phi \mu \Vert_{\mathcal{C}^m}^2 + \kappa \Vert \mu \Vert_{TV} =  \frac{1}{2} \frac{\gamma}{m} \sum_{j=1}^m \vert  y_j - \sigma(\omega_j) \mathcal{F}[\mu](\omega_j) \vert^2 + \kappa \Vert \mu \Vert_{TV} \right\}. \tag{S2Mix}
   \end{equation}

Here :math:`\gamma = (2 \pi)^{-d}` is some constant, independent from
any model parameters (but may depend on the dimension :math:`d`),
ensuring
$:raw-latex:`\mathbb{E}`\ *:raw-latex:`\omega[\frac{\gamma}{m} \Vert \mathcal{A}_m \nu \Vert^2_{\mathbb{C}^m}] `=
:raw-latex:`\Vert `:raw-latex:`\mathcal{F}`[:raw-latex:`\nu`]
:raw-latex:`\Vert`*\ {L\ :sup:`2(:raw-latex:`\Lambda`)}`\ 2 $ and hence
the asymptotic convergence of the S2Mix objective to the one of Supermix
as :math:`m \to + \infty` (law of large numbers).

Conic particle gradient descent
-------------------------------

`Chizat, 2020 <https://hal.science/hal-02190822/document>`__ proposed a
very general framework for solving sparse optimization problem on the
space of measures, such as the BLASSO. The idea is to use a
discretisation of the candidate measure
:math:`\mu_K = \sum_{k=1}^K r_k^2 \delta_{m_k}` and to minimize the
corresponding objective

.. math::


   H_K((r_1, m_1), \ldots, (r_K, m_K)) = J(\mu_K)

by gradient descent on the particles
:math:`(r,m) = ((r_k, m_k))_{k=1, \ldots, K}`. However, Chizat propose
to use a specific Riemannian metric on the space of particles
:math:`(r, m)`, and hence specific Riemannian gradients, allowing to
leverage theoretical convergence guarantees to global optima for this
non-convex discrete problem. More details may be found in the original
article, we only detail the Riemannian gradient of :math:`H_K` w.r.t.
:math:`r_k` and :math:`m_k` which are given for the general case in
Section 2.

.. math::


   \begin{cases}
   \nabla_{r_k} H_K(r,m) = 2 \alpha r_k J'_{\mu_K}(m_k)   \\
   \nabla_{m_k} H_K(r, m)  = \beta \nabla_{m_k} J'_{\mu_K}(m_k)
   \end{cases} \qquad \textrm{with: } \mu_K = \frac{1}{K} \sum_{k=1}^K r_k^2 \delta_{m_k}

with

.. math::


   \begin{cases}
   J'_{\mu_K}(m_k) = \frac{\gamma}{m} \left\{\sum_{j=1}^m  \sigma^2(\omega_j)\sum_{l=1}^K \frac{r_l^2}{K} \cos\left( \omega_j^\top (m_l - m_k)\right) - \sum_{j=1}^m \sigma(\omega_j)\left( Re(y_j) \cos(\omega_j^\top m_k) - Im(y_j) \sin(\omega_j^\top m_k) \right) \right\} + \kappa \\
   \nabla_{m_k} J'_{\mu_K}(m_k) = \frac{\gamma}{m} \left\{\sum_{j=1}^m \omega_j \sigma^2(\omega_j)\sum_{l=1}^K \frac{r_l^2}{K} \sin\left( \omega_j^\top (m_l - m_k)\right) - \sum_{j=1}^m \omega_j \sigma(\omega_j)\left( - Re(y_j) \sin(\omega_j^\top m_k) - Im(y_j) \cos(\omega_j^\top m_k) \right)  \right\}
   \end{cases} \qquad \textrm{with: } \mu_K = \frac{1}{K} \sum_{k=1}^K r_k^2 \delta_{m_k}

The hyper-parameters :math:`\alpha` and :math:`\beta` can be interpreted
as step sizes in the gradient descent
