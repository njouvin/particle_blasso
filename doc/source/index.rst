.. particle_blasso documentation master file, created by
   sphinx-quickstart on Wed May 31 11:24:46 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to particle_blasso's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Documented modules:


   ./mixture.rst
   ./data.rst

Maths background on BLASSO problems
=============

.. toctree::
   :maxdepth: 1
   :caption: Estimating translation invariant mixture models with Beurling-LASSO

   ./latex_s2mix.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
