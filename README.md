# particle_blasso : a Python package to solve Beurling-LASSO using Conic Particle Gradient Descent


## Installation
```bash
git clone git@forgemia.inra.fr:njouvin/particle_blasso.git
cd particle_blasso
pip install .
```

## Get started with the notebooks

The `/Notebooks/` folder contains introductory notebooks illustrating the use of the package.

    * `S2Mix.ipynb` presents the resolution of a Sketched Supermix BLASSO problem to estimate parameters of a translation invariant mixture model

# Contributing

Install pre-commit with pip (mostly black and linting stuff)

```bash
pip install pre-commit
```

And set it up

```bash
pre-commit install
```
# Documentation

A static documentation is available at [https://njouvin.pages.mia.inra.fr/particle_blasso](https://njouvin.pages.mia.inra.fr/particle_blasso)
