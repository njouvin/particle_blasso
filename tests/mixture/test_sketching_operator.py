from particle_blasso.mixture import SketchingOperator
import numpy as np
import pytest

seed = int(np.random.default_rng(None).random() * 1e8)
n, d = 77, 3
m = 11
X = np.random.default_rng(None).normal(size=(n, d))
sketch_ope = SketchingOperator(
    sketching_distribution="Uniform", n_sketches=m, seed=seed, bandwidth=3.5
)


def test_fit():
    sketch_ope.fit_transform(X)
    pass


def test_frequencies_shape():
    assert sketch_ope.frequencies_.shape == (m, d)


def test_sketch_shape():
    assert sketch_ope.data_sketch.shape == (m,)


def test_distribution_not_implemented():
    with pytest.raises(NotImplementedError):
        SketchingOperator(sketching_distribution="XYZ")


def test_case_insensitive_distribution_name():
    SketchingOperator(sketching_distribution="UNiForm")
