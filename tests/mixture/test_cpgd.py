from particle_blasso.data import Gmm
from particle_blasso.mixture import SketchingOperator, SketchedSupermix
import numpy as np
import pytest


# Parameters
n_spikes = 2
d = 4
min_separation = 5
n = int(353)
seed = int(np.random.default_rng().random() * 1e8)

amplitudes = np.repeat(1, n_spikes) / n_spikes
means = np.zeros((n_spikes, d))
means[:, 0] = min_separation * np.arange(n_spikes)
sigmas = np.array([np.eye(d) for _ in range(n_spikes)])
gmm = Gmm(n_spikes=n_spikes, d=d, min_separation=min_separation, seed=seed)
gmm.mixing_law(means=means, sigmas=sigmas, amplitudes=amplitudes)
gmm.sampling(n=n)

# --- Create Sketching Operator
n_sketches = int(7)
sketch_dist = "uniform"
bandwidth = 3.4
uniform_sketcher = SketchingOperator(
    n_sketches=n_sketches,
    bandwidth=bandwidth,
    seed=seed,
    sketching_distribution=sketch_dist,
)
uniform_sketcher.fit_transform(X=gmm.sample)

# --- Create the S2Mix problem
kappa_reg = 5.3e-3
model = "gmm"
blasso = SketchedSupermix(
    sketcher=uniform_sketcher,
    model=model,
    kappa_reg=kappa_reg,
    seed=seed,
    variance=None,  # np.eye(d) by default
)


def test_fit_cpgd_method_with_random_init():
    n_particles = 5
    n_iter = int(10)
    blasso.fit(
        algorithm="cpgd",
        n_particles=n_particles,
        n_iter=n_iter,
        init={"rs": None, "params": {"means": None}},  # random initialization
        mirror_retraction=False,
        step_sizes={"rs": 0.98, "means": 0.023},
    )

    assert blasso.cpgd_.params_["means"].shape == (n_particles, d)
    print(blasso.cpgd_.params_evolution)
    assert len(blasso.cpgd_.params_evolution) == 1
    assert blasso.cpgd_.params_evolution["means"].shape == (
        n_particles,
        d,
        n_iter,
    )
    assert blasso._algorithm.lower() == "cpgd"
